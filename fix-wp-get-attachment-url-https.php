<?php

	/**
	 * Plugin Name: Fix WP Get Attachment URL
	 * Plugin URI: #
	 * Description: The wp_get_attachment_url function always returns a http url, fix that for ssl
	 * Version: 0.1
	 * Author: iamfriendly, CTLT
	 * Author URI: http://ubc.ca/
	 */

	// Simply filter wp_get_attachment_url, check for http and if we're on ssl, replace it
	add_filter( 'wp_get_attachment_url', 'wp_get_attachment_url__fixHTTPS', 99, 2 );


	/**
	 * wp_get_attachment_url always uses HTTP for image paths. Fi that until 4.1
	 * resoves it for us
	 *
	 * @author Richard Tape <@richardtape>
	 * @package Fix WP Get Attachment URL
	 * @since 1.0
	 * @param (string) $url - The URL of the attachment
	 * @param (int) $postID - the ID for where this attachment is attached
	 * @return (string) If on SSL replace http with https, otherwise just http
	 */
	
	function wp_get_attachment_url__fixHTTPS( $url, $postID )
	{

			if( !is_ssl() ){
				return $url;
			}

			$output = preg_replace( "/^http:/i", "https:", $url );
			return $output;

	}/* wp_get_attachment_url__fixHTTPS() */

